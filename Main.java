package GitCLI;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {
    protected static JFrame mainFrame;
    protected static JPanel container;
    protected static JPanel showBoard;
    protected static JTextField projectPath;

    public static void main(String[] args) {
        container = new JPanel();
        mainFrame = new JFrame("Git CLI");

        JPanel inputContainer = new JPanel();
        projectPath = new JTextField(30);
        projectPath.setText("C:\\Users\\Administrator\\Downloads\\git\\lostintaste");
        JButton chooseButton = new JButton("Browse");
        chooseButton.addActionListener(e -> projectPath.setText(chooseFolder()));

        inputContainer.add(projectPath);
        inputContainer.add(chooseButton);

        JPanel navBar = new JPanel();
        showBoard = new JPanel();
        setVertical(showBoard);

        JButton branch = new JButton("Get Branches");
        branch.addActionListener(e -> showBranches());
        navBar.add(branch);

        JButton remotes = new JButton("Get Remotes");
        remotes.addActionListener(e -> showRemotes());
        navBar.add(remotes);

        container.add(inputContainer);
        container.add(navBar);
        container.add(showBoard);
        createFrame();
    }

    public static  void showBranches(){
        showBoard.removeAll();
        String command = "cd " + projectPath.getText() + " && git branch";
        for(String s : executeCommand(command)){
            JPanel holder = new JPanel();
            JTextField content = new JTextField();
            content.setEditable(false);
            content.setText(s);

            JPanel actions = new JPanel();
            actions.setAlignmentX(JPanel.RIGHT_ALIGNMENT);

            JButton detailButton = new JButton("Detail");
            detailButton.addActionListener(e -> {
                String checkoutCommand = "cd " + projectPath.getText() + " && git checkout " + s;
                executeCommand(checkoutCommand);
                showBranchDetail();
            });

            JButton checkoutButton = new JButton("Checkout");
            checkoutButton.addActionListener(e -> {
                String checkoutCommand = "cd " + projectPath.getText() + " && git checkout " + s;
                executeAndRefreshBranches(checkoutCommand, container);
            });

            JButton newButton = new JButton("New");
            newButton.addActionListener(e -> {
                JPanel addDialog = new JPanel();
                JTextField branchName = new JTextField(30);
                addDialog.add(branchName);
                int result = JOptionPane.showConfirmDialog(null, addDialog,
                        "New Branch Name", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    String addCommand = "cd " + projectPath.getText() +
                            ((!s.contains("*")) ? " && git checkout " + s : "") +
                            " && git checkout -b " + branchName.getText();
                    executeAndRefreshBranches(addCommand, container);
                }
            });

            JButton deleteButton = new JButton("Delete");
            deleteButton.addActionListener(e -> {
                int result = JOptionPane.showConfirmDialog(null, null,
                        "Delete This Branch?", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    if(s.contains("*")){
                        JOptionPane.showMessageDialog(null,"This is the Current Branch.");
                        return;
                    }
                    String addCommand = "cd " + projectPath.getText() +
                            " && git branch -D " + s;
                    executeAndRefreshBranches(addCommand, container);
                }
            });

            actions.add(detailButton);
            actions.add(checkoutButton);
            actions.add(newButton);
            actions.add(deleteButton);

            holder.add(content);
            holder.add(actions);

            showBoard.add(holder);
        }
        refresh(mainFrame);
    }

    public static void executeAndRefreshBranches(String command, JPanel container){
        for(String s : executeCommand(command)){
            System.out.println(s);
        }
        showBranches();
        refresh(container);
    }

    public static void showBranchDetail(){
        JPanel branchContainer = new JPanel();
        JFrame branchFrame = new JFrame();
        setVertical(branchContainer);
        JScrollPane scroll = new JScrollPane();

        buildBranchContainer(branchContainer);
        scroll.setViewportView(branchContainer);

        branchFrame.setSize(600, 600);
        branchFrame.setLocationRelativeTo(null);
        branchFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // make program stop when close window
        branchFrame.add(scroll);
        branchFrame.setVisible(true);
    }

    public static void buildBranchContainer(JPanel container){
        container.removeAll();
        container.add(new JLabel("Changes Files"));
        String untrackedFileCommand = "cd " + projectPath.getText() + " && git ls-files . --exclude-standard --others";
        for(String s : executeCommand(untrackedFileCommand)){
            JPanel holder = new JPanel();
            JLabel path = new JLabel(s);

            JButton addButton = new JButton("+");
            addButton.addActionListener(e -> {
                String addCommand = "cd " + projectPath.getText() + " && git add " + s;
                executeAndBuildBranchContainer(addCommand, container);
            });

            holder.add(path);
            holder.add(addButton);
            container.add(holder);
        }
        String modifiedFileCommand = "cd " + projectPath.getText() + " && git diff --name-only";
        for(String s : executeCommand(modifiedFileCommand)){
            JPanel holder = new JPanel();
            JLabel path = new JLabel(s);

            JButton addButton = new JButton("+");
            addButton.addActionListener(e -> {
                String addCommand = "cd " + projectPath.getText() + " && git add " + s;
                executeAndBuildBranchContainer(addCommand, container);
            });

            holder.add(path);
            holder.add(addButton);
            container.add(holder);
        }

        container.add(new JLabel("Added Files"));
        String addedFileCommand = "cd " + projectPath.getText() + " && git diff --cached --name-only";
        for(String s : executeCommand(addedFileCommand)){
            JPanel holder = new JPanel();
            JLabel path = new JLabel(s);

            JButton removeButton = new JButton("-");
            removeButton.addActionListener(e -> {
                String removeCommand = "cd " + projectPath.getText() + " && git restore --staged " + s;
                executeAndBuildBranchContainer(removeCommand, container);
            });

            holder.add(path);
            holder.add(removeButton);
            container.add(holder);
        }
        remoteActions(container);
    }

    public static void remoteActions(JPanel container){
        JPanel actions = new JPanel();
        JButton commit = new JButton("Commit");
        commit.addActionListener(e -> {
            JPanel messageHolder = new JPanel();
            JTextField message = new JTextField(30);
            messageHolder.add(message);
            int result = JOptionPane.showConfirmDialog(null, messageHolder,
                    "Commit Message", JOptionPane.OK_CANCEL_OPTION);
            if (result == JOptionPane.OK_OPTION) {
                String commitCommand = "cd " + projectPath.getText() + " && git commit -m \"" + message.getText() + "\"";
                executeAndBuildBranchContainer(commitCommand, container);
            }
        });

        JButton undoCommit = new JButton("Undo Commit");
        undoCommit.addActionListener(e -> {
            int result = JOptionPane.showConfirmDialog(null, null,
                    "Latest Commit will be Undone!", JOptionPane.OK_CANCEL_OPTION);
            if (result == JOptionPane.OK_OPTION) {
                String undoCommand = "cd " + projectPath.getText() + " && git reset HEAD~";
                executeAndBuildBranchContainer(undoCommand, container);
            }
        });

        JButton pull = new JButton("Pull");
        pull.addActionListener(e -> {
            JPanel remotes = new JPanel();
            String command = "cd " + projectPath.getText() + " && git remote";
            for(String s : executeCommand(command)){
                JButton remote = new JButton(s);
                remote.addActionListener(e2 -> {
                    String pullCommand = "cd " + projectPath.getText() + " && git pull " + remote.getText() +
                            " " + (executeCommand("cd " + projectPath.getText() + " && git branch --show-current")).get(0);
                    executeCommand(pullCommand);
                    JOptionPane.showMessageDialog(null,"Pulled.");
                });
                remotes.add(remote);
            }
            JOptionPane.showConfirmDialog(null, remotes,
                    "Pull", JOptionPane.OK_CANCEL_OPTION);
        });

        JButton push = new JButton("Push");
        push.addActionListener(e -> {
            JPanel remotes = new JPanel();
            String command = "cd " + projectPath.getText() + " && git remote";
            for(String s : executeCommand(command)){
                JButton remote = new JButton(s);
                remote.addActionListener(e2 -> {
                    String pushCommand = "cd " + projectPath.getText() + " && git push " + remote.getText() +
                            " " + (executeCommand("cd " + projectPath.getText() + " && git branch --show-current")).get(0);
                    executeAndBuildBranchContainer(pushCommand, container);
                    JOptionPane.showMessageDialog(null,"Pushed.");
                });
                remotes.add(remote);
            }
            JOptionPane.showConfirmDialog(null, remotes,
                    "Push", JOptionPane.OK_CANCEL_OPTION);
        });

        actions.add(commit);
        actions.add(undoCommit);
        actions.add(pull);
        actions.add(push);

        container.add(actions);
    }

    public static void executeAndBuildBranchContainer(String command, JPanel container){
        for(String s : executeCommand(command)){
            System.out.println(s);
        }
        buildBranchContainer(container);
        refresh(container);
    }

    public static void showRemotes(){
        showBoard.removeAll();
        String command = "cd " + projectPath.getText() + " && git remote";
        for(String s : executeCommand(command)){
            JPanel holder = new JPanel();
            JTextField content = new JTextField();
            content.setEditable(false);
            content.setText(s);

            JButton newButton = new JButton("New");
            newButton.addActionListener(e -> {
                JPanel addDialog = new JPanel();
                JTextField remoteName = new JTextField(30);
                JTextField remoteUrl = new JTextField(30);
                addDialog.add(new JLabel("Name"));
                addDialog.add(remoteName);
                addDialog.add(new JLabel("Url"));
                addDialog.add(remoteUrl);
                int result = JOptionPane.showConfirmDialog(null, addDialog,
                        "New Origin Name", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    String addCommand = "cd " + projectPath.getText() +
                            " && git remote add " + remoteName.getText() + " " + remoteUrl.getText();
                    executeCommand(addCommand);
                    showRemotes();
                    refresh(mainFrame);
                }
            });

            JButton deleteButton = new JButton("Delete");
            deleteButton.addActionListener(e -> {
                int result = JOptionPane.showConfirmDialog(null, null,
                        "Delete This Remote?", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    String removeCommand = "cd " + projectPath.getText() +
                            " && git remote remove " + s;
                    executeCommand(removeCommand);
                    showRemotes();
                    refresh(mainFrame);
                }
            });

            holder.add(content);
            holder.add(newButton);
            holder.add(deleteButton);

            showBoard.add(holder);
        }
        refresh(mainFrame);
    }

    public static ArrayList<String> executeCommand(String command) {
        ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
        builder.redirectErrorStream(true);
        ArrayList<String> result = new ArrayList<>();

        try {
            Process p = builder.start();

            BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;

            while (true) {
                line = r.readLine();
                if (line == null) { break; }
                result.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String chooseFolder(){
        JFileChooser chooser = new JFileChooser();

        chooser.setCurrentDirectory(new File("C:\\Users\\Administrator\\Downloads"));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);

        if (chooser.showOpenDialog(container) == JFileChooser.APPROVE_OPTION) {
            return chooser.getSelectedFile().toString();
        }
        return "";
    }

    public static void setVertical(JPanel panel){
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    }

    public static void createFrame(){
        setVertical(container);
        JScrollPane scroll = new JScrollPane();
        scroll.setViewportView(container);

        mainFrame.setSize(600, 600);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // make program stop when close window
        mainFrame.add(scroll);
        mainFrame.setVisible(true);
    }

    static void refresh(JFrame frame){
        frame.invalidate();
        frame.validate();
        frame.repaint();
    }

    static void refresh(JPanel panel){
        panel.invalidate();
        panel.validate();
        panel.repaint();
    }
}
